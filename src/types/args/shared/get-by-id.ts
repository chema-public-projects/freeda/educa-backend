import { ArgsType, Field, ID } from "type-graphql";

@ArgsType()
export class GetByIdArgs {
  @Field(type => ID)
  id!: string;
}
