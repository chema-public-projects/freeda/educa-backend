
import { Resolver, Args, Query } from "type-graphql";
import { User } from "../../entities";
import { GetByIdArgs } from "../../types/args/shared";

@Resolver()
export class UserResolver {
  @Query(returns => [User])
  public async users(): Promise<User[]> {
    const usersCollection: User[] = [];
    return await usersCollection;
  }

  @Query(returns => [User])
  public async user(@Args()  { id }: GetByIdArgs): Promise<User[]> {
    const usersCollection: User[] = [];
    return await usersCollection;
  }
}
