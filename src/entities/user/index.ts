import { ObjectType, Field, ID } from "type-graphql";

@ObjectType()
export class User {
  @Field(type => ID)
  id!: string;

  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  middleName?: string;

  @Field({ nullable: true })
  lastName?: string;

  @Field({ nullable: true })
  fullName?: string;
}
